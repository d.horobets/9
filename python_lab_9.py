import random


# Ця програма створює файл TF8_1 із 10 символьними рядками
def create_file():
    with open("TF8_1.txt", "w") as f:
        for i in range(10):
            line = "".join([chr(random.randint(ord("a"), ord("z"))) for _ in range(random.randint(1, 20))])
            f.write(line + "\n")


create_file()


# Ця програма читає вміст файла TF8_1 і пропускає цифри.
def read_file(filename):
    with open(filename, "r") as f:
        lines = f.readlines()

    return lines


def process_lines(lines):
    processed_lines = []
    num = 1
    for line in lines:
        line = line.strip()
        new_line = ""
        for c in line:
            if c.isalpha():
                new_line += c
        if len(new_line) > 10:
            new_line = new_line[:10]
        processed_lines.append("%05d %s" % (num, new_line))
        num += 1

    return processed_lines


def write_file(lines, filename):
    with open(filename, "w") as f:
        for line in lines:
            f.write(line + "\n")


lines = read_file("TF8_1.txt")
processed_lines = process_lines(lines)
write_file(processed_lines, "TF8_2.txt")


# Ця програма читає вміст файла TF8_2 і друкує його по рядках.
def read_file(filename):
    with open(filename, "r") as f:
        lines = f.readlines()

    return lines


def print_lines(lines):
    for line in lines:
        print(line.strip())


lines = read_file("TF8_2.txt")
print_lines(lines)
